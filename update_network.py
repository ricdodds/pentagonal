import sys

from utils import *


def do_update_network(from_file=True):
    graph = load_graph(from_file)

    network = []

    visited, queue = set(), [MAIN_LINK]
    while queue:
        link = queue.pop(0)

        for child in graph[link]:
            network.append({
                'source_text': link.text,
                'source_href': link.href,
                'target_text': child.text,
                'target_href': child.href
            })

        if link not in visited:
            visited.add(link)
            queue.extend(graph[link] - visited)

    save_to_file(network, filename='network/data.json')

if __name__ == "__main__":
    do_update_network(from_file='-f' in sys.argv or '--from-file' in sys.argv)
