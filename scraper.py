import bs4
import urllib
import collections


HyperLink = collections.namedtuple('HyperLink', 'text href')

MAIN_LINK = HyperLink(text='pentagonal', href='6.htm')

ROOT_URL = 'http://pendientedemigracion.ucm.es/info/especulo/hipertul/pentagonal/'


class Scraper(object):
    def __call__(self, start):
        visited, queue, graph = set(), [start], {}
        while queue:
            link = queue.pop(0)
            if link not in visited:
                visited.add(link)
                print len(visited), link.text

                graph[link] = set(self.get_hyperlinks(ROOT_URL + link.href))

                queue.extend(graph[link] - visited)

        return graph

    def get_hyperlinks(self, url):
        html = urllib.urlopen(url).read()
        soup = bs4.BeautifulSoup(html, 'html.parser')

        hyperlinks = soup.find_all('a') or soup.find_all('area')

        return [
            HyperLink(text=link.text, href=link.get('href'))
            for link in hyperlinks if 'http' not in link
        ]
