import json
import pickle

from scraper import *


def load_graph(from_file):
    if from_file:
        return pickle.load(open("graph.pkl", "rb"))
    else:
        scraper = Scraper()

        return scraper(start=MAIN_LINK)


def save_to_file(o, filename):
    with open(filename, 'w') as outfile:
        json.dump(o, outfile)
