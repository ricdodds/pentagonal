import sys

from utils import *


def do_update_tree(from_file=True):
    graph = load_graph(from_file)

    tree = {'link': MAIN_LINK}

    visited, queue = set(), [tree]
    while queue:
        node = queue.pop(0)

        if '_children' not in node:
            node['_children'] = []

        for child in graph[node['link']]:
            node['_children'].append({'link': child})

        if node['link'] not in visited:
            visited.add(node['link'])

            queue_links = graph[node['link']] - visited
            queue_nodes = filter(lambda n: n['link'] in queue_links, node['_children'])

            queue.extend(queue_nodes)

    save_to_file([tree], filename='tree/data.json')


if __name__ == "__main__":
    do_update_tree(from_file='-f' in sys.argv or '--from-file' in sys.argv)
